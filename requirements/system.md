Avanzamos en el diseño y estructura de nuestro proyecto, el cual se basa en el control y gestión de residencias de la tercera edad con funciones inteligentes.
 
# SYSTEM BOOK: sistemas y componentes electrónicos necesarios para el proyecto.

S.1 COMPONENTES: partes de Hardware y Software.

S.2.FUNCIONALIDAD: funcionalidad de cada uno de los componentes

S.3 INTERFACES: como se combinan las funcionalidades para obtener los objetivos

S.4 ESCENARIOS DE USO DETALLADOS: ejemplos de interacción humana o con el entorno con los dispositivos. Ejemplos de casos de uso e historias de uso.

S.5 PRIORIZACIÓN: clasificación de los comportamientos interfaces y escenarios por su grado de criticidad.

S.6 VERIFICACIÓN Y CRITERIO DE ACEPTACIÓN: especificación de las especificaciones en las que dicha implementación será satisfactoria.

## S.1 PARTES DE HARDWARE-SOFTWARE

- Micro:bit: compuesto por un ordenador de placa única (SBC) que cuenta con diversos sensores, varias entradas para anexar otros elementos de hardware y conexión inalámbrica mediante Bluetooth. La placa aloja el procesador ARM Cortex-M4 con 128KB de memoria RAM. Además, contiene 512KB de memoria flash (no volátil) en la que se pueden almacenar programas y datos en ficheros.

- Entorno de programación micro:bit: utilizaremos el runtime de la Universidad de Lancaster que viene preinstalado, pero no programaremos en C++. Encima de la API C++ se han desarrollado diferentes entornos de programación en múltiples lenguajes: Python, JavaScript, y varios entornos de programación con bloques gráficos. Nosotros utilizaremos MicroPython.

- Entorno de programación en el servidor a través del puerto serie: programación en lenguaje Python. Podemos utilizar threading para enviar mensajes de forma concurrente y atender a diferentes peticiones de los clientes.

- Entorno virtual a partir de flask para instalar los paquetes Python necesarios de tu proyecto. Podemos generar una aplicación que implementa una API REST para una base de datos.

- Cliente HTTP a través de Python: con la ayuda del paquete request de Python podemos realizar peticiones HTTP de clientes. 

## S.2 FUNCIONALIDAD

- Micro:bit: tenemos un dispositivo con transmisión radio Bluetooth y con más componentes hardware como sensores o botones que nos permitirán reflejar problemas y situaciones a controlar en las residencias. A través del software de dicho dispositivo establecemos qué tipo de datos vamos a controlar.
 
- Servidor atado a un puerto: tenemos un servidor creado de forma concurrente capaz de escuchar peticiones tanto por parte del micro:bit como peticiones HTTP.

- Entorno virtual Flask: nos permitirá tener los paquetes necesarios para el proyecto, así como para implementar la API REST de la base de datos.

- Cliente HTTP: podremos realizar peticiones necesarias para regular los problemas que van surgiendo en la residencia.

## S.3 INTERFACES

- Micro:bit - Micro:bit: tendremos un micro:bit por cada sala encargado de enviar toda la información recogida por los demás dispositivos para enviarla al servidor y procesar dicha información.

- Micro:bit - Servidor: esta comunicación servirá para captar necesidades o peticiones de las personas que porten un micro:bit.

- Peticiones - Servidor: igualmente, sirve para captar necesidades o peticiones de las personas que portan el micro:bit, ya sean enfermeros o pacientes.

- Servidor - Base de datos: en la base de datos recolectamos la información de los pacientes y enfermeros, así como datos relevantes como podría ser la distribución de la medicación.

## S.4 ESCENARIOS DE USO DETALLADOS

- Petición de un paciente para que vengan a atenderle:  
El paciente presionaría el botón que lleva en su micro:bit durante dos segundos o más. Automaticamente el micro:bit transmisor de la sala enviará dicha petición al servidor, este la procesará y enviará la instrucción necesaria al enfermero correspondiente para que vaya a atenderle.  
	 Historias de usuario:  
Como un paciente quiero poder tener configurado un botón para poder llamar al enfermero.  
Como un servidor quiero poder recibir y entender la instrucción para dar la orden adecuada.  
Como un enfermero quiero poder recibir información adecuada para realizar mi trabajo.  

- Enfermero repartiendo la medicación a sus pacientes:   
El enfermero tiene la tarea de repartir la medicación a los pacientes que tiene asignados. Para ello deberá hacer una petición al servidor sobre cual es la medicación necesaria, este accederá a la base de datos de esa residencia en concreto, se accederá a la medicación necesaria para los pacientes que le ha pedido el enfermero y le pasara esta información.  
	Historias de usuario:  
Como un enfermero quiero saber que administrar para no hacer mal mi trabajo.  
Como un cliente quiero que mi residencia tenga bien gestionada la medicación.  
Como cliente quiero que el acceso a la medicación sea sencillo y fácil de entender para no tener equivocaciones.  
Como un servidor quiero poder acceder a la base de datos.

## S.5 PRIORIZACIÓN

Es muy importante también tener el Software sin errores y bien programado para estar preparado para cualquier adversidad o modificación.
Todo el sistema es igual de crítico, necesitamos que se transmita bien la información entre micro:bit y otro micro:bit o servidor, al igual que es importante tener la base de datos bien estructurada y organizada para no tener equivocaciones. El conjunto debe de prestar una alta disponibilidad para tener los menos fallos posibles.
Otro elemento muy crítico es la conexión Bluetooth de todos los dispositivos, debemos asegurarnos de que tenemos conectividad en todo momento.

## S.6 VERIFICACIÓN Y CRITERIO DE ACEPTACIÓN

Para que la implementación sea exitosa y satisfactoria debemos programar de forma correcta todo el componente Software, así como asegurarnos de que ninguno de nuestros dispositivos esta averiado o tiene fallos internos. 
También debemos cerciorarnos de que creamos la base de datos perfectamente adaptada a nuestras necesidades y que se puede modificar en función de lo que necesitamos en todo momento.
El proyecto será válido y aceptado una vez que planteemos el esquema entero de la red, unifiquemos todos sus componentes Hardware y Software para ver un resultado final y hagamos las pruebas y correcciones necesarias para que en la práctica se vea reflejado el resultado esperado teóricamente.
