# THE GOALS BOOK


## G.1 Context and overall objectifs
El cliente nos ha pedido implantar un sistema en las residencias que nos permita recabar informacion acerca de los ancianos para asi poder ayudarles de una forma mejor. El sistema contara con una serie de microbits que llevaran encima tanto los ancianos como el personal de la residencia para que, a traves de ellos, se pueda enviar y recibir informacion, pueda haber comunicacion, etc. Todo esto con el objetivo de ayudar de una manera optima a los ancianos. 


## G.2 Current situation
Actualemente tenemos una idea general de como queremos que sea el sistema sin saber exactamente como implantarlo de la mejor manera posible. En las residencias actualmente t tenemos un sistema arcaico y tradicional.


## G.3 Expected benefits
Una vez el sistema este implantado esperamos tener una mejor respuesta a cualquier necesidad o problema que puedan tener los pacientes, tales como avisarles de que se tomen la medicacion, como asistirles en caso de caida mas rapidamente, saber donde se encuentran en cada momento, avisar de forma rapida si una enfermera necesita ayuda al atender a un paciente, que el paciente pueda avisar de que precisa de la atencion de alguien del personal, etc.


## G.4 Functionaltiy overview
La funcionalidad de los microbits que llevaran tanto pacientes como personal es de alarma, de transmisor y receptor de informacion y de geolocalizador.


## G.5 High level usage scenarios
Como casos de uso del sistema nos podemos encontrar usar el microbit en forma de alarma en el caso que necesitemos avisar al paciente o al personal rapidamente de cualquier evento que tena dicho paciente en su calendario personal guardado en la base de datos tales como la toma de su medicacion, la visita de un familiar, etc.

Tambien nos podemos encontrar la geolocalizacion de los pacientes. El microbit lo usaremos como geolocalizador para saber en todo momento donde se encuentran los pacientes en el caso de que quieran abandonar la residencia o en el caso de que un miembro del personal necesite acudir rapidamente a atender un paciente.

Otro uso del sistema puede ser el guardar en una base de datos toda la informacion relevante de los pacientes para saber su historial medico, saber en que momento se deben tomar la medicacion, en caso de que algun familiar quiera ver el historial de un paciente o en caso de que tengamos que enfrentarnos a algun problema judicial, tengamos todos los movimientos registrados.

El sistema tambien queremos que se use para que el personal pueda tener comunicacion entre si a traves del microbit en caso de necesitar ayuda de otro miembro de dicho personal o para que en caso de que un paciente necesite ayuda se avise a traves del microbit al miembro del personal mas cercano a el.

Como caso de uso del sistema podemos tener tambien funcionalidades en los botones del microbit para que en caso de necesitar ayuda, un paciente pulse y se envie una señal a un miembro del personal mas cercano. El microbit tambien contaria con un acelerometro que nos ayude a detectar si un paciente ha sufrido una caida o si un paciente lleva mas tiempo de lo normal sin moverse.


## G.6 Limitations and exclusions
Por el momento no sabemos que inconveniencias o limitaciones nos vamos a encontrar en el desarrollo del sistema.


## G.7 Stakeholders and requirements sources
#### Detección de problemas o anomalías en los pacientes
- **Como un** paciente 
- **Para que** me solucione un problema
- **Quiero poder** llamar a un miembro del personal

#### Detección de problemas o anomalías en los pacientes
- **Como un** microbit
- **Para que** avisar del problema
- **Quiero poder** detectar un problema

#### Detección de problemas o anomalías en los pacientes
- **Como un** miembro del personal
- **Para que** ayudar al paciente
- **Quiero poder** recibir la información
		
#### Detección de problemas o anomalías en los pacientes
- **Como un** administrador
- **Para que** enviar el mensaje
- **Quiero poder** detectar el problema

#### Detección de problemas o anomalías en los pacientes
- **Como un** administrador
- **Para que** vaya a atender al paciente
- **Quiero poder** enviar el mensaje al miembro del personal

#### Comunicación entre miembros del personal			
- **Como un** miembro del personal
- **Para que** me ayude
- **Quiero poder** llamar a un miembro del personal

#### Comunicación entre miembros del personal
- **Como un** miembro del personal
- **Para que** ayudarle
- **Quiero poder** recibir una llamada de un miembro del personal
		
#### Almacenamiento de información en una base de datos	
- **Como un** miembro del personal
- **Para que** obtener información
- **Quiero poder** acceder al historial de un paciente

#### Almacenamiento de información en una base de datos
- **Como un** administrador
- **Para que** tener un registro
- **Quiero poder** almacenar el historial

#### Almacenamiento de información en una base de datos
- **Como un** administrador
- **Para que** tenerlos identificados
- **Quiero poder** asignar un identificador a los pacientes

#### Almacenamiento de información en una base de datos
- **Como un** familiar
- **Para que** saber como esta
- **Quiero poder** saber información
		
#### Geolocalización
- **Como un** miembro del personal
- **Para que** atenderle
- **Quiero poder** localizar a un paciente

#### Geolocalización
- **Como un** administrador
- **Para que** tenerle controlado
- **Quiero poder** localizar a un paciente
		
#### Microbit alarma
- **Como un** administrador 
- **Para que** sepa cuando se tiene que tomar la medicacion
- **Quiero poder** enviar un mensaje

#### Microbit alarma
- **Como un** miembro del personal
- **Para que** darle la medicación al paciente
- **Quiero poder** recibir un mensaje

#### Microbit alarma
- **Como un** microbit
- **Para que** avisar al paciente
- **Quiero poder** recibir mensajes

	



