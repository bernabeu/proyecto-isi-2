# THE ENVIRONMENT BOOK


## E.1 Glossary
	
micro-bit: Micro-bit es un dispositivo pequeño, portátil y de bajo coste que se utiliza para mejorar la calidad de vida de los ancianos en residencias. Está diseñado para ayudar a los ancianos a mantenerse conectados con sus seres queridos, a mejorar su salud mental y física, y a mantenerse activos. El dispositivo cuenta con una pantalla, un micrófono, un altavoz y una variedad de sensores, entre otros. Esto permite a los usuarios acceder a información y contenido multimedia, enviar alertas de salud y seguridad, y realizar actividades recreativas.
	
	
## E.2 Components

De momento no hay ya que en este punto pondremos las interfaces que vayamos usando en el proyecto.
	
	
## E.3 Constraints
	
El dispositivo debe cumplir con los requisitos de seguridad y privacidad establecidos por la legislación vigente.

Para un proyecto de micro-bit en una residencia de ancianos en España, se debe cumplir con la normativa legal vigente, como la Ley Orgánica 3/2018, de 5 de diciembre, de Protección de Datos Personales y garantía de los derechos digitales, la Ley Orgánica 15/1999, de Protección de Datos de Carácter Personal y la Ley 34/2002, de 11 de julio, de Servicios de la Sociedad de la Información y de Comercio Electrónico. Estas leyes establecen los requisitos para el tratamiento de los datos personales de los usuarios, así como los derechos de los usuarios y los deberes de los responsables del tratamiento de los datos. Además, se debe 	cumplir con la normativa de seguridad y salud en el trabajo, para garantizar la seguridad de los usuarios.
	
	
## E.4 Assumptions

La velocidad de trasmision de las notificaciones de los microbit debe ser lo suficiente mente rapida para atender una urgencia si es necesaria.

Debido al metodo de interconexion entre micro-bits, resulta muy sencillo que dos micro-bits alejados intercambien informacion por la cantidad de usuarios que disponen de un dispositivo.


## E.5 Effects

Que un administrador o un enfermero modifique algun dato del paciente como puede ser la hora de la medicacion.

Es necesario cargar la bateria de los micro-bit cuando esta se agote.


## E.6 Invariants

Cada miembro de la residencia tiene que tener un micro-bit y llevarlo siempre encima/puesto.
	
Cada integrante del personal sanitario debe tener acceso a la informacion de los micro-	bits.
	
	
	
	
