# THE PROJECT BOOK


## P.1 Roles and personnel
Hasta el momento de momento no tenemos definidos que roles tomara el personal (en este caso los alumnos de la clase) en el desarrollo del sistema.


## P.2 Imposed technical choices
Como requisitos impuestos por el cliente tenemos el uso obligatorio de microbits, el uso de sqlite3 para la base de datos, el uso de APIs REST, una tolerancia a fallos en la repeticion de alarmas y en la perdida de alarmas. Esto significa que no nos podemos permitir que avisemos a un miembro del personal y esta llamada les aparezca a distintos miembros ni que esta llamada no le aparezca a ningun miembro. Tambien se nos impone que el sistema este testeado y que la Base-Station sea por USB.


## P.3 Schedule and milestones
Hasta el momento no sabemos fechas en las que debera estar implantado el sistema.


## P.4 Tasks and deliverables
Por el momento no sabemos las tareas que tendra el personal de desarrollo ni las activididades que les seran asignadas a cada uno de sus miembros.


## P.5 Required technology elements
A nivel tecnologico necesitamos los lenguajes de programacion python y MUEditor, asi como sqlite para las bases de datos. Usaremos la herramienta GitLab para poner en comun los avances de cada uno de los miembros del personal de desarrollo. Como elementos de hardware tenemos los microbits o los point-access que implantaremos en cada habitacion de la residencia.


## P.6 Risk and mitigation analysis
Por el momento no sabemos que inconveniencias o riesgos nos vamos a encontrar en el desarrollo del sistema


## P.7 Requirements process and report
El sistema que nos ha pedido el cliente recabara informacion acerca de los pacientes. Este proceso se hara mediante una base de datos de sqlite donde tendremos toda la informacion bien estructurada en tablas para poder localizarla rapidamente.
El sistema tambien contara con una serie de microbits que funcionaran como repetidores de señal, como transmisores y receptores de mensajes. Para ello deberemos hacer un cliente/servidor en python y hacer uso del MU-Editor para programar los microbits.
Tambien haremos uso de la herramienta GitLab en la que tendremos repositorios compartidos con los demas miembros donde iremos guardando todos los avances que vaya habiendo durante el desarrollo.

